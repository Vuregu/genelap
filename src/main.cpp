#include <iostream>
#include <string>
#include <cstring>
#include <parser.hpp>
#include <lexer.hpp>

int main(int argc, char* argv[])
{
    Lexer lexer;
    Parser parser;
    
    std::string lexical_grammar_filename;
    std::string phrase_grammar_filename;
    std::string source_filename;
    
    for (int i = 0; i < argc; ++i)
    {
        if (std::strcmp(argv[i], "-l") == 0)
        {
            if (i + 1 < argc)
            {
                lexical_grammar_filename = argv[i + 1];
            }
        }
        else if (std::strcmp(argv[i], "-p") == 0)
        {
            if (i + 1 < argc)
            {
                phrase_grammar_filename = argv[i + 1];
            }
        }
        else if (std::strcmp(argv[i], "-s") == 0)
        {
            if (i + 1 < argc)
            {
                source_filename = argv[i + 1];
            }
        }
    }
    
    if (lexical_grammar_filename.empty())
    {
        std::cout << "No lexical grammar file specified, please use -l lexical_grammar_filename." << std::endl;
    }
    else
    {
        lexer.load_lexical_grammar(lexical_grammar_filename);
    }
    
    if (phrase_grammar_filename.empty())
    {
        std::cout << "No phrase grammar file specified, please use -p phrase_grammar_filename." << std::endl;
    }
    else
    {
        parser.load_phrase_grammar(phrase_grammar_filename);
        parser.construct_parsing_table();
    }
    
    if (source_filename.empty())
    {
        std::cout << "No source file specified, please use -s source_filename." << std::endl;
    }
    else
    {
        lexer.generate_token_stream(source_filename);
    }
    
    if (!lexical_grammar_filename.empty() && !phrase_grammar_filename.empty() && !source_filename.empty())
    {
        if (parser.parse(lexer.token_stream) == ParsingState::ACCEPTED)
        {
            parser.generate_dot();
        }
    }
    
    return 0;
}

