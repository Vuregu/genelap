/* 
 * File:   parser.cpp
 * Author: Vreg Andreasyan
 *
 * Created on December 15, 2014, 15:18
 */

#include <parser.hpp>
#include <sstream>
#include <algorithm>

Node::~Node()
{
    for (std::vector< Node* >::iterator child = this->children.begin(); child != this->children.end(); ++child)
    {
        delete *child;
    }
}

Parser::Parser(std::string phrase_grammar_filename)
{
    this->load_phrase_grammar(phrase_grammar_filename);
    this->construct_parsing_table();
}

PhraseGrammarLoadingState Parser::load_phrase_grammar(std::string phrase_grammar_filename)
{
    this->symbols.empty();
    
    std::fstream phrase_grammar(phrase_grammar_filename, std::ios::in);
    if (phrase_grammar.is_open())
    {
        std::string delim = " -> ";
        int delim_pos;
        
        std::string line;
        while (getline(phrase_grammar, line))
        {
            if ((delim_pos = line.find(delim)) > 0)
            {
                SymbolName symbol_name = line.substr(0, delim_pos);
                Production production = line.substr(delim_pos + delim.length(), line.length() - symbol_name.length() - delim.length() - 1);
                
                Symbol new_symbol(symbol_name);
                
                for (std::vector< Symbol >::iterator symbol = this->symbols.begin(); symbol != this->symbols.end(); ++symbol)
                {
                    if (symbol->name.compare(symbol_name) == 0)
                    {
                        symbol->addProduction(production);
                        goto next_symbol;
                    }
                }
                
                new_symbol.addProduction(production);
                this->symbols.push_back(new_symbol);
                
                next_symbol:
                continue;
            }
        }
        
        phrase_grammar.close();
        
        std::cout << "Parser: Successfully loaded specified phrase grammar." << std::endl;
        this->loaded_phrase_grammar = true;
        return PhraseGrammarLoadingState::SUCCESSFULLY_LOADED_PHRASE_GRAMMAR;
    }
    else
    {
        std::cout << "Parser: Loading failed, could not read specified phrase grammar file." << std::endl;
        this->loaded_phrase_grammar = false;
        return PhraseGrammarLoadingState::LOADING_PHRASE_GRAMMAR_FAILED;
    }
}

std::vector< Terminal > Parser::first(Production production, bool& empty)
{
    if (this->loaded_phrase_grammar)
    {
        std::vector< Terminal > first_terminals;
        std::vector< Production > productions;
        productions.push_back(production);
        
        while (!productions.empty())
        {
            std::vector< Production > new_productions;
            
            for (std::vector< Production >::iterator production = productions.begin(); production != productions.end(); ++production)
            {
                if ((*production)[0] == '[')
                {
                    first_terminals.push_back(production->substr(0, production->find(']') + 1));
                }
                else if ((*production)[0] == '<')
                {
                    SymbolName first_symbol = production->substr(0, production->find('>') + 1);
                    
                    for (std::vector< Symbol >::iterator symbol = this->symbols.begin(); symbol != this->symbols.end(); ++symbol)
                    {
                        if (symbol->name.compare(first_symbol) == 0)
                        {
                            for (std::vector< Production >::iterator sub_production = symbol->productions.begin(); sub_production != symbol->productions.end(); ++sub_production)
                            {
                                if (sub_production->compare("[empty]") != 0)
                                {
                                    Production new_production = *sub_production + production->substr(production->find('>') + 1);
                                    new_productions.push_back(new_production);
                                }
                                else
                                {
                                    Production new_production = production->substr(production->find('>') + 1);
                                    if (!new_production.empty())
                                    {
                                        new_productions.push_back(new_production);
                                    }
                                    else
                                    {
                                        empty = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            productions = new_productions;
        }
        
        return first_terminals;
    }
    else
    {
        std::cout << "Parser: Could not determine first, no phrase grammar loaded." << std::endl;
        return std::vector< Terminal >();
    }
}

std::vector< Terminal > Parser::follow(SymbolName symbol_name, std::vector< SymbolName >* followed_symbols)
{
    if (this->loaded_phrase_grammar)
    {
        bool bottom_follow = false;
        
        if (followed_symbols == NULL)
        {
            followed_symbols = new std::vector< SymbolName >;
            bottom_follow = true;
        }
        
        followed_symbols->push_back(symbol_name);
        
        std::vector< Terminal > following_terminals;
        
        if (symbol_name.compare("<start>") == 0)
        {
            following_terminals.push_back("[EOS]");
        }
        
        for (std::vector< Symbol >::iterator symbol = this->symbols.begin(); symbol != this->symbols.end(); ++symbol)
        {
            for (std::vector< Production >::iterator production = symbol->productions.begin(); production != symbol->productions.end(); ++production)
            {
                if (production->compare("[empty]") != 0)
                {
                    Production production_copy = *production;
                    int symbol_pos;
                    
                    while ((symbol_pos = production_copy.find(symbol_name)) != -1)
                    {
                        if (symbol_pos + symbol_name.length() != production_copy.length())
                        {
                            production_copy = production_copy.substr(symbol_pos + symbol_name.length());
                            bool empty = false;
                            
                            std::vector< Terminal > first_terminals = this->first(production_copy, empty);
                            following_terminals.insert(following_terminals.end(), first_terminals.begin(), first_terminals.end());
                            
                            if (empty)
                            {
                                if (std::find(followed_symbols->begin(), followed_symbols->end(), symbol->name) == followed_symbols->end())
                                {
                                    std::vector< Terminal > follow_terminals = this->follow(symbol->name, followed_symbols);
                                    following_terminals.insert(following_terminals.end(), follow_terminals.begin(), follow_terminals.end());
                                }
                            }
                        }
                        else
                        {
                            if (std::find(followed_symbols->begin(), followed_symbols->end(), symbol->name) == followed_symbols->end())
                            {
                                std::vector< Terminal > follow_terminals = this->follow(symbol->name, followed_symbols);
                                following_terminals.insert(following_terminals.end(), follow_terminals.begin(), follow_terminals.end());
                            }
                            
                            break;
                        }
                    }
                }
            }
        }
        
        if (bottom_follow)
        {
            delete followed_symbols;
        }
        
        return following_terminals;
    }
    else
    {
        std::cout << "Parser: Could not determine follow, no phrase grammar loaded." << std::endl;
        return std::vector< Terminal >();
    }
}

ParsingTableConstructionState Parser::construct_parsing_table()
{
    this->parsing_table.clear();
    
    if (this->loaded_phrase_grammar)
    {
        for (std::vector< Symbol >::iterator symbol = this->symbols.begin(); symbol != this->symbols.end(); ++symbol)
        {
            std::pair< SymbolName, std::vector< std::pair< Terminal, Production > > > parsing_table_row(symbol->name, std::vector< std::pair< Terminal, Production > >());
            
            for (std::vector< Production >::iterator production = symbol->productions.begin(); production != symbol->productions.end(); ++production)
            {
                std::vector< Terminal > terminals;
                
                if (production->compare("[empty]") != 0)
                {
                    bool empty = false;
                    
                    terminals = this->first(*production, empty);
                    
                    if (empty)
                    {
                        std::vector< Terminal > follow_terminals = this->follow(symbol->name);
                        terminals.insert(terminals.end(), follow_terminals.begin(), follow_terminals.end());
                    }
                }
                else
                {
                    terminals = this->follow(symbol->name);
                }
                
                for (std::vector< Terminal >::iterator terminal = terminals.begin(); terminal != terminals.end(); ++terminal)
                {
                    std::vector< std::pair< Terminal, Production > >::iterator parsing_table_column = parsing_table_row.second.begin();
                    for (parsing_table_column; parsing_table_column !=  parsing_table_row.second.end(); ++parsing_table_column)
                    {
                        if (parsing_table_column->first.compare(*terminal) == 0)
                        {
                            if (parsing_table_column->second.compare(*production) == 0)
                            {
                                goto next_terminal;
                            }
                            else
                            {
                                std::cout << "Parser: Could not construct parsing table, the loaded phrase grammar is non-deterministic." << std::endl;
                                std::cout << "Parser: Non-determinism location:" << std::endl;
                                std::cout << "Symbol: " << symbol->name << std::endl;
                                std::cout << "Terminal: " << *terminal << std::endl;
                                std::cout << "Production #1: " << parsing_table_column->second << std::endl;
                                std::cout << "Production #2: " << *production << std::endl;
                                
                                this->constructed_parsing_table = false;
                                return ParsingTableConstructionState::NON_DETERMINISM_DETECTED;
                            }
                        }
                    }
                    
                    parsing_table_row.second.push_back(std::pair< Terminal, Production >(*terminal, *production));
                    
                    next_terminal:
                    continue;
                }
            }
            
            this->parsing_table.push_back(parsing_table_row);
        }
        
        std::cout << "Parser: Successfully constructed parsing table from the loaded phrase grammar." << std::endl;
        this->constructed_parsing_table = true;
        return ParsingTableConstructionState::SUCCESSFULLY_CONSTRUCTED;
    }
    else
    {
        std::cout << "Parser: Could not construct parsing table, no phrase grammar loaded." << std::endl;
        this->constructed_parsing_table = false;
        return ParsingTableConstructionState::NO_PHRASE_GRAMMAR_LOADED;
    }
}

void Parser::print_parsing_table()
{
    if (this->constructed_parsing_table)
    {
        std::vector< std::pair< SymbolName, std::vector< std::pair< Terminal, Production > > > >::iterator parsing_table_row = this->parsing_table.begin();
        for (parsing_table_row; parsing_table_row != this->parsing_table.end(); ++parsing_table_row)
        {
            std::vector< std::pair< Terminal, Production > >::iterator parsing_table_column = parsing_table_row->second.begin();
            for (parsing_table_column; parsing_table_column != parsing_table_row->second.end(); ++parsing_table_column)
            {
                std::cout << parsing_table_row->first << " with terminal " << parsing_table_column->first << " produces " << parsing_table_column->second << std::endl;
            }
        }
    }
    else
    {
        std::cout << "Parser: Could not print parsing table, no parsing table constructed." << std::endl;
    }
}

Production Parser::get_production(SymbolName symbol_name, Terminal terminal)
{
    if (this->constructed_parsing_table)
    {
        std::vector< std::pair< SymbolName, std::vector< std::pair< Terminal, Production > > > >::iterator parsing_table_row = this->parsing_table.begin();
        for (parsing_table_row; parsing_table_row != this->parsing_table.end(); ++parsing_table_row)
        {
            if (parsing_table_row->first.compare(symbol_name) == 0)
            {
                std::vector< std::pair< Terminal, Production > >::iterator parsing_table_column = parsing_table_row->second.begin();
                for (parsing_table_column; parsing_table_column != parsing_table_row->second.end(); ++parsing_table_column)
                {
                    if (parsing_table_column->first.compare(terminal) == 0)
                    {
                        return parsing_table_column->second;
                    }
                }
            }
        }
        
        return "NO_PRODUCTION_FOUND";
    }
    else
    {
        std::cout << "Parser: Could not get the production of " << symbol_name << " for " << terminal << ", no parsing table constructed." << std::endl;
        return "NO_PRODUCTION_FOUND";
    }
}

ParsingState Parser::parse(std::vector< Token > token_stream)
{
    this->root_node.~Node();
    this->root_node.item = "<start>";
    this->root_node.item_type = "[symbol]";
    this->root_node.children.clear();
    
    this->parsing_stack.clear();
    this->parsing_stack.push_back(&this->root_node);
    
    if (this->constructed_parsing_table)
    {
        std::vector< Token >::iterator token = token_stream.begin();
        while (!this->parsing_stack.empty())
        {
            if (this->parsing_stack.back()->item_type.compare("[symbol]") == 0)
            {
                Production production = this->get_production(this->parsing_stack.back()->item, token->first);
                
                if (production.compare("NO_PRODUCTION_FOUND") != 0)
                {
                    if (production.compare("[empty]") != 0)
                    {
                        std::vector< Node* > nodes;
                        
                        while (!production.empty())
                        {
                            if (production[0] == '<')
                            {
                                int last_char_pos = production.find('>');

                                nodes.push_back(new Node());
                                nodes.back()->item = production.substr(0, last_char_pos + 1);
                                nodes.back()->item_type = "[symbol]";

                                production = production.substr(last_char_pos + 1);
                            }
                            else if (production[0] == '[')
                            {
                                int last_char_pos = production.find(']');

                                nodes.push_back(new Node());
                                nodes.back()->item_type = production.substr(0, last_char_pos + 1);

                                production = production.substr(last_char_pos + 1);
                            }
                            else
                            {
                                std::cout << "Parser: Parsing failed, an invalid production has been detected." << std::endl;
                                std::cout << "Parser: Invalid production location:" << std::endl;
                                std::cout << this->get_production(this->parsing_stack.back()->item, token->first) << std::endl;
                                this->parsed_token_stream = false;
                                return ParsingState::INVALID_PRODUCTION;
                            }
                        }
                        
                        this->parsing_stack.back()->children = nodes;
                        this->parsing_stack.pop_back();
                        
                        for (std::vector< Node* >::iterator node = nodes.end() - 1; node != nodes.begin() - 1; --node)
                        {
                            this->parsing_stack.push_back(*node);
                        }
                    }
                    else
                    {
                        this->parsing_stack.back()->children.push_back(new Node());
                        this->parsing_stack.back()->children.back()->item_type = "[empty]";
                        this->parsing_stack.pop_back();
                    }
                }
                else
                {
                    std::cout << "Parser: Parsing failed, the provided token stream has been rejected." << std::endl;
                    this->parsed_token_stream = false;
                    return ParsingState::REJECTED;
                }
            }
            else
            {
                if (this->parsing_stack.back()->item_type.compare(token->first) == 0)
                {
                    this->parsing_stack.back()->item = token->second;
                    this->parsing_stack.pop_back();
                    token++;
                }
                else
                {
                    std::cout << "Parser: Parsing failed, the provided token stream has been rejected." << std::endl;
                    this->parsed_token_stream = false;
                    return ParsingState::REJECTED;
                }
            }
        }
        
        std::cout << "Parser: Successfully parsed the provided token stream." << std::endl;
        this->parsed_token_stream = true;
        return ParsingState::ACCEPTED;
    }
    else
    {
        std::cout << "Parser: Could not parse the provided token stream, no parsing table constructed." << std::endl;
        this->parsed_token_stream = false;
        return ParsingState::NO_PARSING_TABLE_CONSTRUCTED;
    }
}

void Parser::generate_dot()
{
    std::ofstream output;
    output.open("parse_tree.dot");
    output << "digraph {" << std::endl;
    output << "node [shape=record]" << std::endl;
    
    this->node_to_dot(&this->root_node, output);
    
    output << "}" << std::endl;
    output.close();
    
    std::cout << "Generated DOT visualization of the parse tree." << std::endl;
}

void Parser::node_to_dot(Node* node, std::ofstream& output)
{
    output << "\"" << node << "\" [label=\"{ " << this->format_to_dot_label(node->item_type) << " | " << this->format_to_dot_label(node->item) << " }\"]" << std::endl;
    for (std::vector<Node*>::iterator child = node->children.begin(); child != node->children.end(); ++child)
    {
        output << "\"" << node << "\" -> \"" << *child << "\"" << std::endl;
        this->node_to_dot(*child, output);
    }
}

std::string Parser::format_to_dot_label(std::string& to_format)
{
    std::stringstream ss;
    for (std::string::iterator character = to_format.begin(); character != to_format.end(); ++character)
    {
        if (!std::isalnum(*character))
        {
            ss << "\\";
        }
        
        ss << *character;
    }
    
    return ss.str();
}

