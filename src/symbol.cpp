/* 
 * File:   symbol.cpp
 * Author: Vreg Andreasyan
 *
 * Created on December 10, 2014, 3:25
 */

#include <symbol.hpp>

typedef std::string SymbolName;
typedef std::string Terminal;
typedef std::string Production;

Symbol::Symbol(SymbolName name)
{
    this->name = name;
}

void Symbol::addProduction(Production production)
{
    this->productions.push_back(production);
}

