/* 
 * File:   lexer.hpp
 * Author: Vreg Andreasyan
 *
 * Created on December 29, 2014, 1:02
 */

#include <lexer.hpp>
#include <iostream>
#include <fstream>
#include <boost/regex.hpp>
#include <sstream>

KeywordTokenType Lexer::create_keyword_token(std::string token_definition)
{
    KeywordTokenType keyword_token;
    
    std::string delim = " -> ";
    int delim_pos;
    
    if ((delim_pos = token_definition.find(delim)) > 0)
    {
        keyword_token.token_type = token_definition.substr(0, delim_pos);
        
        if (token_definition.length() > delim_pos + delim.length())
        {
            token_definition = token_definition.substr(delim_pos + delim.length());
            
            if ((delim_pos = token_definition.find(delim)) > 0)
            {
                keyword_token.token_regex = token_definition.substr(0, delim_pos);
                
                if (token_definition.length() > delim_pos + delim.length())
                {
                    keyword_token.preceded_by = token_definition.substr(delim_pos + delim.length());
                }
            }
            else if (delim_pos == -1)
            {
                keyword_token.token_regex = token_definition;
            }
        }
    }
    
    return keyword_token;
}

Lexer::Lexer(std::string lexical_grammar_filename)
{
    this->load_lexical_grammar(lexical_grammar_filename);
}

Lexer::Lexer(std::string lexical_grammar_filename, std::string source_filename)
{
    this->load_lexical_grammar(lexical_grammar_filename);
    this->generate_token_stream(source_filename);
}

LexicalGrammarLoadingState Lexer::load_lexical_grammar(std::string lexical_grammar_filename)
{
    this->keyword_token_types.empty();
    this->user_token_types.empty();
    
    std::fstream lexical_grammar(lexical_grammar_filename, std::ios::in);
    if (lexical_grammar.is_open())
    {
        std::string delim = " <- ";
        int delim_pos;
        
        std::string line;
        while (getline(lexical_grammar, line))
        {
            if ((delim_pos = line.find(delim)) > 1)
            {
                UserTokenType user_token;
                user_token.token_type = line.substr(0, delim_pos);
                
                if (line.length() > delim_pos + delim.length())
                {
                    user_token.followed_by = this->create_keyword_token(line.substr(delim_pos + delim.length(), line.length() - user_token.token_type.length() - delim.length() - 1));
                    
                    if (!user_token.followed_by.token_type.empty() && !user_token.followed_by.token_regex.empty())
                    {
                        this->user_token_types.push_back(user_token);
                    }
                }
            }
            else if (delim_pos == -1)
            {
                KeywordTokenType keyword_token = this->create_keyword_token(line.substr(0, line.size() - 1));
                
                if (!keyword_token.token_type.empty() && !keyword_token.token_regex.empty())
                {
                    this->keyword_token_types.push_back(keyword_token);
                }
            }
        }
        
        lexical_grammar.close();
        
        std::cout << "Lexer: Successfully loaded specified lexical grammar." << std::endl;
        this->loaded_lexical_grammar = true;
        return LexicalGrammarLoadingState::SUCCESSFULLY_LOADED_LEXICAL_GRAMMAR;
    }
    else
    {
        std::cout << "Lexer: Loading failed, could not read specified lexical grammar file." << std::endl;
        this->loaded_lexical_grammar = false;
        return LexicalGrammarLoadingState::LOADING_LEXICAL_GRAMMAR_FAILED;
    }
}

TokenStreamGenerationState Lexer::generate_token_stream(std::string source_filename)
{
    this->token_stream.clear();
    
    if (this->loaded_lexical_grammar)
    {
        std::fstream source(source_filename, std::ios::in);
        if (source.is_open())
        {
            std::stringstream ss;
            ss << source.rdbuf();
            std::string processed_source = ss.str();
            
            boost::match_results<std::string::const_iterator> match_results;
            
            while (!processed_source.empty())
            {
                std::vector< KeywordTokenType >::iterator keyword_token_type = this->keyword_token_types.begin();
                std::vector< UserTokenType >::iterator user_token_type = this->user_token_types.begin();
                
                for (keyword_token_type; keyword_token_type != this->keyword_token_types.end(); ++keyword_token_type)
                {
                    if (!keyword_token_type->preceded_by.empty())
                    {
                        if (this->latest_keyword_token_type.compare(keyword_token_type->preceded_by) != 0)
                        {
                            continue;
                        }
                    }
                    
                    boost::regex regex(keyword_token_type->token_regex);
                    if (boost::regex_search(processed_source, match_results, regex))
                    {
                        if (match_results.position() == 0)
                        {
                            this->token_stream.push_back(Token(keyword_token_type->token_type, match_results[1].str()));
                            this->latest_keyword_token_type = keyword_token_type->token_type;
                            processed_source = processed_source.substr(match_results.length());
                            
                            goto next_process_source_iter;
                        }
                    }
                }
                
                for (user_token_type; user_token_type != this->user_token_types.end(); ++user_token_type)
                {
                    if (!user_token_type->followed_by.preceded_by.empty())
                    {
                        if (this->latest_keyword_token_type.compare(user_token_type->followed_by.preceded_by) != 0)
                        {
                            continue;
                        }
                    }
                    
                    boost::regex regex(user_token_type->followed_by.token_regex);
                    if (boost::regex_search(processed_source, match_results, regex))
                    {
                        this->token_stream.push_back(Token(user_token_type->token_type, processed_source.substr(0, match_results.position())));
                        this->token_stream.push_back(Token(user_token_type->followed_by.token_type, match_results[1]));
                        this->latest_keyword_token_type = user_token_type->followed_by.token_type;
                        processed_source = processed_source.substr(match_results.position() + match_results.length());
                        
                        goto next_process_source_iter;
                    }
                }
                
                if (boost::regex_match(processed_source, boost::regex("\\s+")))
                {
                    processed_source.clear();
                }
                else
                {
                    std::cout << "Lexer: Generation of token stream failed, grammatical error in source." << std::endl;
                    std::cout << "Lexer: Grammatical error location:" << std::endl;
                    std::cout << processed_source;
                    this->generated_token_stream = false;
                    return TokenStreamGenerationState::GRAMMATICAL_ERROR;
                }
                
                next_process_source_iter:
                continue;
            }
        }
        else
        {
            std::cout << "Lexer: Generation of token stream failed, could not read specified source file." << std::endl;
            this->generated_token_stream = false;
            return TokenStreamGenerationState::COULD_NOT_READ_SOURCE_FILE;
        }
        
        this->token_stream.push_back(Token("[EOS]", ""));
        std::cout << "Lexer: Successfully generated token stream from the specified source file." << std::endl;
        this->generated_token_stream = true;
        return TokenStreamGenerationState::SUCCESSFULLY_GENERATED;
    }
    else
    {
        std::cout << "Lexer: Generation of token stream failed, no lexical grammar loaded." << std::endl;
        this->generated_token_stream = false;
        return TokenStreamGenerationState::NO_LEXICAL_GRAMMAR_LOADED;
    }
}

void Lexer::print_token_stream()
{
    if (this->generated_token_stream)
    {
        for (std::vector< Token >::iterator token = this->token_stream.begin(); token != this->token_stream.end(); ++token)
        {
            std::cout << token->first << ":" << token->second << std::endl;
        }
    }
    else
    {
        std::cout << "Lexer: Could not print token stream, no token stream generated." << std::endl;
    }
}

