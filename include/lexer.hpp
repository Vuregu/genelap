/* 
 * File:   lexer.hpp
 * Author: Vreg Andreasyan
 *
 * Created on December 29, 2014, 0:56
 */

#ifndef LEXER_H
#define	LEXER_H

#include <vector>
#include <string>

typedef std::string TokenType;
typedef std::string TokenRegex;
typedef std::string TokenValue;
typedef std::pair< TokenType, TokenValue > Token;

enum LexicalGrammarLoadingState
{
    SUCCESSFULLY_LOADED_LEXICAL_GRAMMAR,
    LOADING_LEXICAL_GRAMMAR_FAILED
};

enum TokenStreamGenerationState
{
    SUCCESSFULLY_GENERATED,
    COULD_NOT_READ_SOURCE_FILE,
    NO_LEXICAL_GRAMMAR_LOADED,
    GRAMMATICAL_ERROR
};

struct KeywordTokenType
{
    TokenType token_type;
    TokenRegex token_regex;
    TokenType preceded_by;
};

struct UserTokenType
{
    TokenType token_type;
    KeywordTokenType followed_by;
};

class Lexer
{
private:
    // List of this lexical grammar's keyword token types.
    std::vector< KeywordTokenType > keyword_token_types;
    
    // List of this lexical grammar's user tokens types.
    std::vector< UserTokenType > user_token_types;
    
    // Latest keyword token type.
    TokenType latest_keyword_token_type;
    
    // Creates a keyword token out of the specified token definition.
    KeywordTokenType create_keyword_token(std::string token_definition);
    
public:
    // Token stream.
    std::vector< Token > token_stream;
    
    // Did we load a lexical grammar?
    bool loaded_lexical_grammar = false;
    
    // Did we generate a token stream?
    bool generated_token_stream = false;
    
    // Default constructor.
    Lexer(){};
    
    // Initializing constructor.
    Lexer(std::string lexical_grammar_filename);
    
    // Initializing constructor.
    Lexer(std::string lexical_grammar_filename, std::string source_filename);
    
    // Loads the lexical grammar.
    LexicalGrammarLoadingState load_lexical_grammar(std::string lexical_grammar_filename);
    
    // Generates a tokenstream from the specified source file.
    TokenStreamGenerationState generate_token_stream(std::string source_filename);
    
    // Prints the token stream.
    void print_token_stream();
};

#endif	/* LEXER_H */

