/* 
 * File:   parser.hpp
 * Author: Vreg Andreasyan
 *
 * Created on December 15, 2014, 15:18
 */

#ifndef PARSER_H
#define	PARSER_H

#include <iostream>
#include <fstream>
#include <vector>
#include <utility>
#include <string>
#include <symbol.hpp>

typedef std::string SymbolName;
typedef std::string Terminal;
typedef std::string Production;
typedef std::string TokenType;
typedef std::string TokenValue;
typedef std::pair< TokenType, TokenValue > Token;

enum ParsingTableConstructionState
{
    PRODUCTION_GOES_EMPTY,
    SUCCESSFULLY_CONSTRUCTED,
    NON_DETERMINISM_DETECTED,
    NO_PHRASE_GRAMMAR_LOADED
};

enum PhraseGrammarLoadingState
{
    SUCCESSFULLY_LOADED_PHRASE_GRAMMAR,
    LOADING_PHRASE_GRAMMAR_FAILED
};

enum ParsingState
{
    ACCEPTED,
    REJECTED,
    INVALID_PRODUCTION,
    NO_PARSING_TABLE_CONSTRUCTED
};

struct Node
{
    std::string item;
    std::string item_type;
    std::vector< Node* > children;
    
    ~Node();
};

class Parser
{
private:
    // Parsing table.
    std::vector< std::pair< SymbolName, std::vector< std::pair< Terminal, Production > > > > parsing_table;
    
public:
    // Symbols.
    std::vector< Symbol > symbols;
    
    // Root node of the parse tree.
    Node root_node;
    
    // Parsing stack;
    std::vector< Node* > parsing_stack;
    
    // Did we load a phrase grammar?
    bool loaded_phrase_grammar = false;
    
    // Did we construct a parsing table?
    bool constructed_parsing_table = false;
    
    // Did we parse a token stream?
    bool parsed_token_stream = false;
    
    // Default constructor.
    Parser(){};
    
    // Initializing constructor.
    Parser(std::string phrase_grammar_filename);
    
    // Loads the phrase grammar.
    PhraseGrammarLoadingState load_phrase_grammar(std::string phrase_grammar_filename);
    
    // Returns the first terminal(s) of the specified production.
    std::vector< Terminal > first(Production production, bool& empty);
    
    // Returns the following terminal(s) of the specified symbol.
    std::vector< Terminal > follow(SymbolName symbol_name, std::vector< SymbolName >* followed_symbols = NULL);
    
    // Constructs the parsing table.
    ParsingTableConstructionState construct_parsing_table();
    
    // Prints the parsing table.
    void print_parsing_table();
    
    // Returns the production of a symbol for the specified terminal.
    Production get_production(SymbolName symbol_name, Terminal terminal);
    
    // Parses the provided token stream.
    ParsingState parse(std::vector< Token > token_stream);
    
    // Generates DOT visualization of the parse tree.
    void generate_dot();
    
    // Recursively converts a node into DOT and then proceeds to its children.
    void node_to_dot(Node* node, std::ofstream& output);
    
    // Formats a given string into a valid DOT label.
    std::string format_to_dot_label(std::string& to_format);
};

#endif	/* PARSER_H */

