/* 
 * File:   symbol.hpp
 * Author: Vreg Andreasyan
 *
 * Created on December 10, 2014, 3:25
 */

#ifndef SYMBOL_H
#define	SYMBOL_H

#include <vector>
#include <string>

typedef std::string SymbolName;
typedef std::string Terminal;
typedef std::string Production;

class Symbol
{
public:
    // Name of the symbol.
    SymbolName name;
    
    // Productions of the symbol.
    std::vector< Production > productions;
    
    // Default constructor.
    Symbol(){};
    
    // Initializing constructor.
    Symbol(SymbolName name);
    
    // Adds a production.
    void addProduction(Production production);
};

#endif	/* SYMBOL_H */

